from django.test import TestCase, Client
from django.urls import resolve
from lab_5.views import index, matkul_delete_form, matkul_create_form, matkul_specific_id
from lab_5.models import Matkul
from django.apps import apps
from lab_5.apps import Lab5Config


class Lab5Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab5Config.name, 'lab_5')
        self.assertEqual(apps.get_app_config('lab_5').name, 'lab_5')

    def test_lab_5_url_is_exist(self):
        response = Client().get('/matakuliah/')
        self.assertEqual(response.status_code, 200)

    def test_lab_5_create_url_is_exist(self):
        response = Client().get('/matakuliah/create/')
        self.assertEqual(response.status_code, 200)

    def test_lab_5_index_using_to_do_list_template(self):
        response = Client().get('/matakuliah/')
        self.assertTemplateUsed(response, 'table/table.html')

    def test_lab_5_create_using_to_do_list_template(self):
        response = Client().get('/matakuliah/create/')
        self.assertTemplateUsed(response, 'form/form.html')

    # def test_lab_6_create_kegiatan_using_to_do_list_template(self):
    #     response = Client().get('/daftar-kegiatan/create-kegiatan/')
    #     self.assertTemplateUsed(response, 'form_kegiatan/form_kegiatan.html')

    # def test_lab_6_create_attendee_using_to_do_list_template(self):
    #     response = Client().get(
    #         '/daftar-kegiatan/create-attendee/<str:id>', kwargs={'id': 1})
    #     self.assertTemplateUsed(response, 'form_attendee/form_attendee.html')

    def test_lab_5_using_index_func(self):
        found = resolve('/matakuliah/')
        self.assertEqual(found.func, index)

    def test_lab_5_using_create_kegiatan_func(self):
        found = resolve('/matakuliah/create/')
        self.assertEqual(found.func, matkul_create_form)

    # def test_lab_6_using_create_attendee_func(self):
    #     found = resolve(
    #         '/daftar-kegiatan/create-attendee/<str:id>', kwargs={'id': 1})
    #     self.assertEqual(found.func, create_attendee)

    def test_model_can_create_new_person(self):
        new_activity = Matkul.objects.create(
            nama="Lily was a little girl", dosen='Pak Jawro', sks="4", deskripsi="Apa aja", semester="Gasal 2020", ruang="R.2010")

        counting_all_available_matkul = Matkul.objects.all().count()
        self.assertEqual(counting_all_available_matkul, 1)

    # def test_model_can_create_new_kegiatan(self):
    #     new_activity = Kegiatan.objects.create(
    #         nama="Lily was a little girl", deskripsi='Russia', waktu="15.00-16.00")

    #     counting_all_available_kegiatan = Kegiatan.objects.all().count()
    #     self.assertEqual(counting_all_available_kegiatan, 1)
