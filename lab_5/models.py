from django.db import models

# Create your models here.


class Matkul (models.Model):
    nama = models.CharField(max_length=50, null=True)
    dosen = models.CharField(max_length=50, null=True)
    sks = models.IntegerField(null=True)
    deskripsi = models.TextField(max_length=500, null=True)
    semester = models.TextField(max_length=100, null=True)
    ruang = models.CharField(max_length=50, null=True)
