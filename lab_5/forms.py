from django import forms
from .models import Matkul


class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = {
            'dosen',
            'sks',
            'deskripsi',
            'ruang',
            'nama',
            'semester'
        }
