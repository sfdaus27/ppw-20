from django.shortcuts import render
from lab_5.models import Matkul
from lab_5.forms import MatkulForm
from django.shortcuts import redirect
# Create your views here.


def index(request):
    data = {}
    matkuls = Matkul.objects.all()
    data['datas'] = matkuls

    return render(request, 'table/table.html', data)


def matkul_create_form(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = MatkulForm()

    context = {
        'form': form
    }
    return render(request, 'form/form.html', context)


def matkul_delete_form(request, id):
    data = Matkul.objects.get(id=id)
    if request.method == "POST":
        data.delete()
        return redirect('lab_5:index')


def matkul_specific_id(request, id):
    data = Matkul.objects.get(id=id)
    context = {
        'data': data
    }
    return render(request, 'detail/detail.html', context)
