from django.urls import path

from . import views

app_name = "lab_5"

urlpatterns = [
    path('', views.index, name='index'),
    path('create/', views.matkul_create_form, name='create'),
    path('delete/<str:id>/', views.matkul_delete_form, name='delete'),
    path('detail/<str:id>/', views.matkul_specific_id, name='detail')
]
