from django.test import TestCase, Client
from django.urls import resolve
from lab_8.views import index
from django.apps import apps
from lab_8.apps import Lab8Config

# Create your tests here.


class Lab7Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab8Config.name, 'lab_8')
        self.assertEqual(apps.get_app_config('lab_8').name, 'lab_8')

    def test_lab_8_url_is_exist(self):
        response = Client().get('/daftar-buku/')
        self.assertEqual(response.status_code, 200)

    def test_lab_8_index_using_to_do_list_template(self):
        response = Client().get('/daftar-buku/')
        self.assertTemplateUsed(response, 'daftarBuku/daftarBuku.html')

    def test_lab_8_using_index_func(self):
        found = resolve('/daftar-buku/')
        self.assertEqual(found.func, index)
