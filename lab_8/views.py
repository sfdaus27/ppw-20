from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
# Create your views here.


def index(request):
    data = requests.get(
        'https://www.googleapis.com/books/v1/volumes?q=Norwegianwood')
    data = data.json()
    data = data['items']
    context = {
        'data': data
    }
    return render(request, 'daftarBuku/daftarBuku.html', context)


def search(request):
    if request.POST.get('post_note', None) is not None:
        post_note = request.POST['post_note']
        url = "https://www.googleapis.com/books/v1/volumes?q=" + post_note
        data = requests.get(url)
        data = data.json()
        data = data['items']
        context = {
            'data': data
        }
    return JsonResponse(context, safe=False)
