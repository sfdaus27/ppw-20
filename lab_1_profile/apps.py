from django.apps import AppConfig


class Lab1ProfileConfig(AppConfig):
    name = 'lab_1_profile'
