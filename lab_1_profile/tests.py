from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from lab_1_profile.apps import Lab1ProfileConfig


class Lab3Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab1ProfileConfig.name, 'lab_1_profile')
        self.assertEqual(apps.get_app_config(
            'lab_1_profile').name, 'lab_1_profile')

    def test_lab_1_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_1_profile_using_to_do_list_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile/profile.html')
