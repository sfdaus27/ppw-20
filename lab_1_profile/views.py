from django.shortcuts import render


def index(request):
    content = {
        'nama': 'Sukma Firdaus',
        'npm': '1606875831',
        'angkatan': '2016',
        'kampus': 'Universitas Indonesia',
        'hobi': 'Santai',
        'deskripsi': 'Mahasiswa Ilmu Komputer angkatan 2016, yang sedang mempelajari dunia "per-website-an"'
    }
    return render(request, 'profile/profile.html', content)
