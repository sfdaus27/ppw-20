from django.urls import path

from . import views

app_name = 'lab_1_profile'

urlpatterns = [
    path('', views.index, name='index'),
]
