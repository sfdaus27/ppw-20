from django.urls import path

from . import views

app_name = "lab_3"

urlpatterns = [
    path('', views.index, name='index'),
    path('more/', views.more, name='more')
]
