from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from lab_3.apps import Lab3Config


class Lab3Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab3Config.name, 'lab_3')
        self.assertEqual(apps.get_app_config('lab_3').name, 'lab_3')

    def test_lab_3_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab_3_create_url_is_exist(self):
        response = Client().get('/more/')
        self.assertEqual(response.status_code, 200)

    def test_lab_3_index_using_to_do_list_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'welcome/welcome.html')

    def test_lab_3_create_using_to_do_list_template(self):
        response = Client().get('/more/')
        self.assertTemplateUsed(response, 'profile-more/profile-more.html')
