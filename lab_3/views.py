from django.shortcuts import render


def index(request):
    return render(request, 'welcome/welcome.html')


def more(request):
    return render(request, 'profile-more/profile-more.html')
