from django.db import models

# Create your models here.


class Person (models.Model):
    nama = models.CharField(max_length=50, null=True)
    domisili = models.CharField(max_length=50, null=True)


class Kegiatan (models.Model):
    nama = models.CharField(max_length=50, null=True)
    deskripsi = models.TextField(max_length=500, null=True)
    waktu = models.CharField(max_length=50, null=True)
    persons = models.ManyToManyField(Person, blank=True)
