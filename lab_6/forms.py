from django import forms
from .models import Kegiatan, Person


class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = {
            'nama',
            'deskripsi',
            'waktu',
            'persons'
        }


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = {
            'nama',
            'domisili'
        }
