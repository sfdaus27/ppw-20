from django.urls import path

from . import views

app_name = "lab_6"

urlpatterns = [
    path('', views.index, name='index'),
    path('create-kegiatan/', views.create_kegiatan, name='create-kegiatan'),
    path('create-attendee/<str:id>/',
         views.create_attendee, name='create-attendee')

]
