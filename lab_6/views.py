from django.shortcuts import render
from lab_6.models import Kegiatan, Person
from lab_6.forms import KegiatanForm, PersonForm
from django.shortcuts import redirect

# Create your views here.


def index(request):
    kegiatans = Kegiatan.objects.all()
    context = {
        'kegiatan': kegiatans
    }
    return render(request, 'kegiatan/kegiatan.html', context)


def create_kegiatan(request):
    form = KegiatanForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = KegiatanForm()

    context = {
        'form': form
    }
    return render(request, 'form_kegiatan/form_kegiatan.html', context)


def create_attendee(request, id):
    data = Kegiatan.objects.filter(id=id)
    test = Kegiatan.objects.all()
    print(test.first().persons.all())
    form = PersonForm(request.POST or None)
    if form.is_valid():
        form.save()
        data.first().persons.add(Person.objects.last())
        return redirect('lab_6:index')

    context = {
        'form': form
    }
    return render(request, 'form_attendee/form_attendee.html', context)
