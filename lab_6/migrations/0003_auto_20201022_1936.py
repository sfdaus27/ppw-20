# Generated by Django 2.2 on 2020-10-22 12:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_6', '0002_person'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='kegiatan',
        ),
        migrations.AddField(
            model_name='kegiatan',
            name='persons',
            field=models.ManyToManyField(to='lab_6.Person'),
        ),
    ]
