from django.test import TestCase, Client
from django.urls import resolve
from lab_6.views import index, create_kegiatan, create_attendee
from lab_6.models import Person, Kegiatan
from django.apps import apps
from lab_6.apps import Lab6Config


class Lab6Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab6Config.name, 'lab_6')
        self.assertEqual(apps.get_app_config('lab_6').name, 'lab_6')

    def test_lab_6_url_is_exist(self):
        response = Client().get('/daftar-kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_create_kegiatan_url_is_exist(self):
        response = Client().get('/daftar-kegiatan/create-kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_create_attendee_url_is_exist(self):
        response = Client().get(
            '/daftar-kegiatan/create-attendee/<str:id>',  kwargs={'id': 1})
        self.assertEqual(response.status_code, 301)

    def test_lab_6_index_using_to_do_list_template(self):
        response = Client().get('/daftar-kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')

    def test_lab_6_create_kegiatan_using_to_do_list_template(self):
        response = Client().get('/daftar-kegiatan/create-kegiatan/')
        self.assertTemplateUsed(response, 'form_kegiatan/form_kegiatan.html')

    # def test_lab_6_create_attendee_using_to_do_list_template(self):
    #     response = Client().get(
    #         '/daftar-kegiatan/create-attendee/<str:id>', kwargs={'id': 1})
    #     self.assertTemplateUsed(response, 'form_attendee/form_attendee.html')

    def test_lab_6_using_index_func(self):
        found = resolve('/daftar-kegiatan/')
        self.assertEqual(found.func, index)

    def test_lab_6_using_create_kegiatan_func(self):
        found = resolve('/daftar-kegiatan/create-kegiatan/')
        self.assertEqual(found.func, create_kegiatan)

    def test_model_can_create_new_person(self):
        new_activity = Person.objects.create(
            nama="Lily was a little girl", domisili='Russia')

        counting_all_available_person = Person.objects.all().count()
        self.assertEqual(counting_all_available_person, 1)

    def test_model_can_create_new_kegiatan(self):
        new_activity = Kegiatan.objects.create(
            nama="Lily was a little girl", deskripsi='Russia', waktu="15.00-16.00")

        counting_all_available_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(counting_all_available_kegiatan, 1)

    # def test_can_save_a_POST_request_kegiatan(self):
    #     response = self.client.post('/daftar-kegiatan/create-kegiatan/', data={
    #                                 'nama': 'asd', 'deskripsi': 'Maen Dota Kayaknya Enak', 'waktu': '16.00-17.00'})

    # def test_can_save_a_POST_request(self):
    #     response = self.client.post('/daftar-kegiatan/create-attendee/', data={'date': '2017-10-12T14:14', 'activity' : 'Maen Dota Kayaknya Enak'})
    #     counting_all_available_activity = Diary.objects.all().count()
    #     self.assertEqual(counting_all_available_activity, 1)

    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'], '/lab-3/')

    #     new_response = self.client.get('/lab-3/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('Maen Dota Kayaknya Enak', html_response)
