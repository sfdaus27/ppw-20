"""lab_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
# from django.urls import include, path
from django.conf.urls import url, include

urlpatterns = [
    url(r'^profile/', include('lab_1_profile.urls', namespace="profile")),
    url(r'^', include('lab_3.urls', namespace="welcome")),
    url(r'^matakuliah/', include('lab_5.urls', namespace="table")),
    url(r'^daftar-kegiatan/', include('lab_6.urls', namespace="daftar-kegiatan")),
    url(r'^accordion/', include('lab_7.urls', namespace="accordion")),
    url(r'^daftar-buku/', include('lab_8.urls', namespace="daftarBuku")),
    url(r'^account/', include('lab_9.urls', namespace="account")),
    url(r'^admin/', admin.site.urls),
]
