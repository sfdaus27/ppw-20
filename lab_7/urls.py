from django.urls import path

from . import views

app_name = "lab_7"

urlpatterns = [
    path('', views.index, name='index')
]
