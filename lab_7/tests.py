from django.test import TestCase, Client
from django.urls import resolve
from lab_7.views import index
from django.apps import apps
from lab_7.apps import Lab7Config

# Create your tests here.


class Lab7Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab7Config.name, 'lab_7')
        self.assertEqual(apps.get_app_config('lab_7').name, 'lab_7')

    def test_lab_7_url_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_index_using_to_do_list_template(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'accordion/accordion.html')

    def test_lab_7_using_index_func(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, index)
