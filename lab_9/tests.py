from django.test import TestCase, Client
from django.urls import resolve
from lab_9.views import index
from django.apps import apps
from lab_9.apps import Lab9Config

# Create your tests here.


class Lab9Test(TestCase):
    def test_apps(self):
        self.assertEqual(Lab9Config.name, 'lab_9')
        self.assertEqual(apps.get_app_config('lab_9').name, 'lab_9')

    # def test_lab_9_url_is_exist(self):
    #     response = Client().get('/account/')
    #     self.assertEqual(response.status_code, 200)

    def test_lab_9_login_url_is_exist(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_register_url_is_exist(self):
        response = Client().get('/account/register/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_using_index_func(self):
        found = resolve('/account/')
        self.assertEqual(found.func, index)
