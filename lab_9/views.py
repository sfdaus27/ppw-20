from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def index(request):
    return render(request, 'welcoming/welcoming.html')


def registerViews(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        print(form)
        if form.is_valid():
            form.save()
            return redirect('lab_9:login')
    else:
        form = UserCreationForm()
    return render(request, 'register/register.html', {'form': form})


def loginViews(request):
    return render(request, 'login/login.html')
